<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => '$2y$10$l2gmCKqFYZ8.uP/IznTMD.XlBq7l5n.Q8erG2zELVV1QhU1bdkxom', // password
        'l10n' => 'my',
        'timezone' => 'Asia/Kuala_Lumpur',
        'remember_token' => rand(),
        'confirmation_code' => rand(),
        'email_verified_at' => now(),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
