<?php

namespace App\Http\Middleware;

use Closure;
use Flash;
use Illuminate\Auth\Middleware\Authenticate;

class Authenticated extends Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param array $guards
     * @return mixed
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($request, $guards);
        $user = auth()->user();
        $manager = app('impersonate');

         /**
          * if
          *     user is not impersonating
          *     and expired date less then today
          *     and email_verified_at is not null
          * then
          * change password
          */
        if ($manager->isImpersonating() == false && $user->expired_date < date('Y-m-d') && $user->email_verified_at != null) {
            Flash::error('Please fill up and submit your document submission');
            return redirect()->route('user.showChangePasswordForm');
        }

        return $next($request);
    }
}
