<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\AppBaseController;
use App\Repositories\AnnouncementRepository;
use App\Repositories\Portal\BannerRepository;
use App\Repositories\RunnerRepository;
use MyOpensoft\Runner\Runner;

class Controller extends AppBaseController
{
    public function __construct()
    {

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $visitor_counter = Runner::generate('{raw}', 'visitor_counter');

        return view('portal.index', compact('visitor_counter'));
    }
}
