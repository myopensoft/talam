<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $hasher;

    /**
     * Create a new controller instance.
     *
     * @param $hasher
     */
    public function __construct(Hasher $hasher)
    {
        $this->middleware('guest');
        $this->hasher = $hasher;
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        // If user reset their password,
        // then 'soft' reset their remember token to flush all existed token.
        $user->setRememberToken(Str::random(60));

        // If user reset their password,
        // then update their password expired_date too.
        $user->expired_date = Carbon::parse()->addYear()->toDateString();

        // If user can reset their password,
        // then assume they want to be verified too.
        if($user->email_verified_at == null) {
            $user->email_verified_at = Carbon::now();
        }

        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);

        switch (config('app.type')) {
            case 'INTERNAL':
                return redirect(route('infra.home'));
                break;
            case 'PUBLIC':
            default:
                return redirect('/public');
                break;
        }
    }

    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        $reset = PasswordReset::where('email', $request->email)->first();

        if($reset) {
            if($this->hasher->check($request->token, $reset->token)) {
                $user = User::where('email', '=', $request->email)->first();
                $user->password = Hash::make($request->password);
                $user->save();

                return $this->sendResetResponse($request, 'Katalaluan anda berjaya dikemaskini. Sila log masuk.');
            } else {
                return $this->sendResetFailedResponse($request, 'Kata laluan anda tidak berjaya dikemaskini.');
            }
        }
        return $this->sendResetFailedResponse($request, 'Kata laluan anda tidak berjaya dikemaskini.');
    }
}
