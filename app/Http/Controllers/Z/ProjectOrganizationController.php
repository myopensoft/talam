<?php

namespace App\Http\Controllers\Z;

use App\Http\Requests\Z\CreateProjectOrganizationRequest;
use App\Http\Requests\Z\UpdateProjectOrganizationRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Z\ProjectOrganization;
use Illuminate\Http\Request;
use Flash;

class ProjectOrganizationController extends AppBaseController
{
    /**
     * Display a listing of the ProjectOrganization.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        /** @var ProjectOrganization $projectOrganizations */
        $projectOrganizations = ProjectOrganization::all();

        return view('z.project_organizations.index')
            ->with('projectOrganizations', $projectOrganizations);
    }

    /**
     * Show the form for creating a new ProjectOrganization.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('z.project_organizations.create');
    }

    /**
     * Store a newly created ProjectOrganization in storage.
     *
     * @param CreateProjectOrganizationRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateProjectOrganizationRequest $request)
    {
        $input = $request->all();

        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::create($input);

        Flash::success('Project Organization saved successfully.');

        return redirect(route('z.projectOrganizations.index'));
    }

    /**
     * Display the specified ProjectOrganization.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::find($id);

        if (empty($projectOrganization)) {
            Flash::error('Project Organization not found');

            return redirect(route('z.projectOrganizations.index'));
        }

        return view('z.project_organizations.show')->with('projectOrganization', $projectOrganization);
    }

    /**
     * Show the form for editing the specified ProjectOrganization.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::find($id);

        if (empty($projectOrganization)) {
            Flash::error('Project Organization not found');

            return redirect(route('z.projectOrganizations.index'));
        }

        return view('z.project_organizations.edit')->with('projectOrganization', $projectOrganization);
    }

    /**
     * Update the specified ProjectOrganization in storage.
     *
     * @param int $id
     * @param UpdateProjectOrganizationRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateProjectOrganizationRequest $request)
    {
        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::find($id);

        if (empty($projectOrganization)) {
            Flash::error('Project Organization not found');

            return redirect(route('z.projectOrganizations.index'));
        }

        $projectOrganization->fill($request->all());
        $projectOrganization->save();

        Flash::success('Project Organization updated successfully.');

        return redirect(route('z.projectOrganizations.index'));
    }

    /**
     * Remove the specified ProjectOrganization from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::find($id);

        if (empty($projectOrganization)) {
            Flash::error('Project Organization not found');

            return redirect(route('z.projectOrganizations.index'));
        }

        $projectOrganization->delete();

        Flash::success('Project Organization deleted successfully.');

        return redirect(route('z.projectOrganizations.index'));
    }
}
