<?php

namespace App\Http\Controllers\API\Z;

use App\Http\Requests\API\Z\CreateClientAPIRequest;
use App\Http\Requests\API\Z\UpdateClientAPIRequest;
use App\Models\Z\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class ClientController
 * @package App\Http\Controllers\API\Z
 */

class ClientAPIController extends AppBaseController
{
    /**
     * Display a listing of the Client.
     * GET|HEAD /clients
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function index(Request $request)
    {
        $query = Client::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $clients = $query->get();

        return $this->sendResponse($clients->toArray(), 'Clients retrieved successfully');
    }

    /**
     * Store a newly created Client in storage.
     * POST /clients
     *
     * @param CreateClientAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function store(CreateClientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Client $client */
        $client = Client::create($input);

        return $this->sendResponse($client->toArray(), 'Client saved successfully');
    }

    /**
     * Display the specified Client.
     * GET|HEAD /clients/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($id)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            return $this->sendError('Client not found');
        }

        return $this->sendResponse($client->toArray(), 'Client retrieved successfully');
    }

    /**
     * Update the specified Client in storage.
     * PUT/PATCH /clients/{id}
     *
     * @param int $id
     * @param UpdateClientAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($id, UpdateClientAPIRequest $request)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            return $this->sendError('Client not found');
        }

        $client->fill($request->all());
        $client->save();

        return $this->sendResponse($client->toArray(), 'Client updated successfully');
    }

    /**
     * Remove the specified Client from storage.
     * DELETE /clients/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            return $this->sendError('Client not found');
        }

        $client->delete();

        return $this->sendSuccess('Client deleted successfully');
    }
}
