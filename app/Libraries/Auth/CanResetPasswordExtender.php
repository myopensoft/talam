<?php

namespace App\Libraries\Auth;

use Illuminate\Auth\Passwords\CanResetPassword;

trait CanResetPasswordExtender
{
    use CanResetPassword;

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->username;
    }
}
