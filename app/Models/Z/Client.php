<?php

namespace App\Models\Z;

use App\Models\Project\Project;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Client
 * @package App\Models\Z
 * @version March 11, 2020, 3:48 am +08
 *
 * @property \App\Models\Z\ProjectType parent
 * @property \Illuminate\Database\Eloquent\Collection projects
 * @property string name
 * @property integer parent_id
 */
class Client extends Model
{
    use SoftDeletes;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
    public $table = 'z_clients';
    public $fillable = [
        'name',
        'parent_id'
    ];
    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'parent_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parent()
    {
        return $this->belongsTo(Client::class, 'parent_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function children()
    {
        return $this->hasMany(Client::class, 'parent_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function projects()
    {
        return $this->hasMany(Project::class, 'client_id')->withTrashed();
    }

}
