<?php

namespace App\Models\Z;

use App\Models\Project\Contract;
use App\Models\Project\Credential;
use App\Models\Project\Hardware;
use App\Models\Project\Software;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProjectType
 * @package App\Models\Z
 * @version March 11, 2020, 3:49 am +08
 *
 * @property \Illuminate\Database\Eloquent\Collection zClients
 * @property \Illuminate\Database\Eloquent\Collection zProjectOrganizations
 * @property \Illuminate\Database\Eloquent\Collection projectContracts
 * @property \Illuminate\Database\Eloquent\Collection projectCredentials
 * @property \Illuminate\Database\Eloquent\Collection projectHardwares
 * @property \Illuminate\Database\Eloquent\Collection projectSoftwares
 * @property string name
 * @property integer parent_id
 */
class ProjectType extends Model
{
    use SoftDeletes;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
    public $table = 'z_project_types';
    public $fillable = [
        'name',
        'parent_id'
    ];
    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'parent_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function clients()
    {
        return $this->hasMany(Client::class, 'parent_id')->withTrashed();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function organizations()
    {
        return $this->hasMany(ProjectOrganization::class, 'parent_id')->withTrashed();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function contracts()
    {
        return $this->hasMany(Contract::class, 'type_id')->withTrashed();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function credentials()
    {
        return $this->hasMany(Credential::class, 'type_id')->withTrashed();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function hardwares()
    {
        return $this->hasMany(Hardware::class, 'type_id')->withTrashed();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function softwares()
    {
        return $this->hasMany(Software::class, 'type_id')->withTrashed();
    }

}
