<?php

namespace App\Models\Z;

use App\Models\Project\Team;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProjectOrganization
 * @package App\Models\Z
 * @version March 11, 2020, 3:49 am +08
 *
 * @property \App\Models\Z\ProjectType parent
 * @property \Illuminate\Database\Eloquent\Collection projectTeams
 * @property string name
 * @property integer parent_id
 */
class ProjectOrganization extends Model
{
    use SoftDeletes;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
    public $table = 'z_project_organizations';
    public $fillable = [
        'name',
        'parent_id'
    ];
    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'parent_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parent()
    {
        return $this->belongsTo(ProjectType::class, 'parent_id')->withTrashed();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function teams()
    {
        return $this->hasMany(Team::class, 'organization_id')->withTrashed();
    }

}
