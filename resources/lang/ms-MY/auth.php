<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Maklumat akaun anda tidak tepat.',
    'throttle' => 'Terlalu banyak percubaan. Sila cuba kembali dalama masa :seconds saat.',

    // button
    'forget_password' => 'Lupa Kata Laluan',
    'register' => 'Daftar Pengguna',
    'register_form' => [
        'box1-title' => 'Maklumat pengguna',
        'box1-subtitle' => 'Sila penuhkan maklumat yang diperlukan.',
        'box2-title' => 'Maklumat identiti',
        'box2-subtitle' => 'Sila isi salah satu daripada maklumat ini.',
        'box3-title' => 'Maklumat perhubungan',
        'box3-subtitle' => 'Sila lengkapkan maklumat yang diperlukan. Sebarang notifikasi akan diberikan melalui email atau sms kepada nombor telefon yang diberikan.',
        'box4-title' => 'Maklumat kata laluan',
        'box4-subtitle' => 'Pastikan kata laluan anda memenuhi keperluan <b>minimum</b> sistem ini.
        <ul>
            <li>8 karakter</li>
            <li>1 huruf</li>
            <li>1 nombor</li>
            <li>1 karakter khas</li>
        </ul>',
        'box5-title' => 'Captcha',
        'box5-subtitle' => 'Sila isi maklumat captcha untuk pengesahan.).'
    ],
    'login' => 'Log Masuk',
];
