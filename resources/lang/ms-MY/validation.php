<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute perlu diterima.',
    'active_url' => ':attribute bukan URL yang sah.',
    'after' => ':attribute perlulah tarikh selepas tarikh :date.',
    'after_or_equal' => ':attribute perlulah tarikh selepas atau sama dengan tarikh :date.',
    'alpha' => ':attribute hanya boleh mengandungi huruf.',
    'alpha_dash' => ':attribute hanya boleh mengandungi huruf, nombor, dash dan underscore.',
    'alpha_num' => ':attribute hanya boleh mengandungi huruf and nombor.',
    'array' => ':attribute perlulah array.',
    'before' => ':attribute perlulah tarikh sebelum tarikh :date.',
    'before_or_equal' => ':attribute perlulah tarikh sebelum atau sama dengan tarikh :date.',
    'between' => [
        'numeric' => ':attribute perlulah di antara :min dan :max.',
        'file' => ':attribute perlulah di antara :min and :max kilobyte.',
        'string' => ':attribute perlulah di antara :min and :max aksara.',
        'array' => ':attribute perlulah di antara :min and :max item.',
    ],
    'boolean' => ':attribute perlulah antara ya atau tidak.',
    'confirmed' => ':attribute pengesahan tidak sah.',
    'date' => ':attribute bukan tarikh yang sah.',
    'date_format' => ':attribute tidak mengikut format :format.',
    'different' => ':attribute dan :other perlulah berlainan.',
    'digits' => ':attribute perlulah mempunyai :digits digit.',
    'digits_between' => ':attribute perlulah di antara :min dan :max digit.',
    'dimensions' => ':attribute mempunyai dimensi yang salah.',
    'distinct' => ':attribute mempunyai data yang sama.',
    'email' => ':attribute perlulah alamat email yang sah.',
    'exists' => ':attribute yang dipilih tidak sah.',
    'file' => ':attribute perlulah sebuah fail.',
    'filled' => ':attribute perlu mempunyai nilai.',
    'gt' => [
        'numeric' => ':attribute perlulah lebih besar daripada :value.',
        'file' => ':attribute perlulah lebih besar daripada :value kilobyte.',
        'string' => ':attribute perlulah lebih besar daripada :value aksara.',
        'array' => ':attribute perlulah lebih banyak daripada :value item.',
    ],
    'gte' => [
        'numeric' => ':attribute perlulah lebih besar daripada atau sama dengan :value.',
        'file' => ':attribute perlulah lebih besar daripada atau sama dengan :value kilobyte.',
        'string' => ':attribute perlulah lebih besar daripada atau sama dengan :value aksara.',
        'array' => ':attribute perlulah lebih banyak atau sama dengan :value item.',
    ],
    'image' => ':attribute perlulah imej.',
    'in' => ':attribute yang dipilih tidak sah.',
    'in_array' => ':attribute tidak sah di dalam :other.',
    'integer' => ':attribute perlulah dalam bentuk integer.',
    'ip' => ':attribute perlulah alamat IP yang sah.',
    'ipv4' => ':attribute perlulah alamat IPV4 yang sah.',
    'ipv6' => ':attribute perlulah alamat IPV6 yang sah.',
    'json' => ':attribute perlulah JSON yang sah.',
    'lt' => [
        'numeric' => ':attribute kurang daripada :value.',
        'file' => ':attribute kurang daripada :value kilobyte.',
        'string' => ':attribute kurang daripada :value character.',
        'array' => ':attribute must have less than :value item.',
    ],
    'lte' => [
        'numeric' => ':attribute kurang daripada atau sama dengan :value.',
        'file' => ':attribute kurang daripada atau sama dengan :value kilobyte.',
        'string' => ':attribute kurang daripada atau sama dengan :value aksara.',
        'array' => ':attribute tidak melebihi daripada :value item.',
    ],
    'max' => [
        'numeric' => ':attribute perlulah tidak lebih daripada :max.',
        'file' => ':attribute perlulah tidak lebih daripada :max kilobyte.',
        'string' => ':attribute perlulah tidak lebih daripada :max aksara.',
        'array' => ':attribute perlulah tidak melebihi :max item.',
    ],
    'mimes' => ':attribute perlulah berjenis: :values.',
    'mimetypes' => ':attribute perlulah berjenis: :values.',
    'min' => [
        'numeric' => ':attribute perlulah sekurang-kurangnya :min.',
        'file' => ':attribute perlulah sekurang-kurangnya :min kilobyte.',
        'string' => ':attribute perlulah sekurang-kurangnya :min aksara.',
        'array' => ':attribute perlulah sekurang-kurangnya :min item.',
    ],
    'not_in' => ':attribute yang dipilih tidak sah.',
    'not_regex' => ':attribute format tidak sah.',
    'numeric' => ':attribute perlulah nombor.',
    'present' => ':attribute perlulah ada.',
    'regex' => ':attribute format tidak sah.',
    'required' => ':attribute adalah wajib',
    'required_if' => ':attribute adalah wajib apabila :other adalah :value.',
    'required_unless' => ':attribute adalah wajib melainkan :other adalah :values.',
    'required_with' => ':attribute adalah wajib apabila :values ada.',
    'required_with_all' => ':attribute adalah wajib apabila :values ada.',
    'required_without' => ':attribute adalah wajib apabila :values tiada.',
    'required_without_all' => ':attribute adalah wajib apabila tiada daripada :values adalah hadir.',
    'same' => ':attribute dan :other perlulah sama.',
    'size' => [
        'numeric' => ':attribute perlulah :size.',
        'file' => ':attribute perlulah :size kilobyte.',
        'string' => ':attribute perlulah :size aksara.',
        'array' => ':attribute perlu mempunyai :size item.',
    ],
    'string' => ':attribute perlulah a string.',
    'timezone' => ':attribute perlulah a valid zone.',
    'unique' => ':attribute telah diambil.',
    'uploaded' => ':attribute gagal dimuatnaik.',
    'url' => ':attribute format tidak sah.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

    /*
     * Pwned password validator
     */
    'pwned' => 'Kata laluan anda terlalu lemah. Sila tukar kata laluan anda.',
    'captcha' => 'Captcha anda salah. Sila cuba lagi.',
];
