<?php

return [
    'emails' => [
        'emails' => [
            'model' => 'Alamat Emel',
            'index' => 'Senarai alamat emel',
            'create' => 'Cipta baharu'
        ],
        'details' => [
            'model' => 'Data Emel',
            'index' => 'Senarai data emel',
            'create' => 'Cipta baharu'
        ],
    ],
    'smses' => [
        'sms' => [
            'model' => 'SMS',
            'index' => 'Senarai SMS',
            'create' => 'Cipta baharu'
        ],
        'details' => [
            'model' => 'Data Emel',
            'index' => 'Senarai data emel',
            'create' => 'Cipta baharu'
        ],
    ],
];
