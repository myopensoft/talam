<?php

return [
    'model' => 'Pengguna',
    'index' => 'Senarai pengguna',
    'create' => 'Cipta pengguna baharu',
    'show' => 'Lihat',
    'edit' => 'Kemaskini',
    'update' => 'Simpan',
    'delete' => 'Hapus',
    'note_1' => [
        'title' => 'Maklumat Pengguna',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatibus, deserunt est debitis. Unde
            earum consectetur, voluptatem quo minus facere vitae, dolor nostrum tenetur nemo voluptatibus! Provident
            nihil, sit harum?'
    ],
    'note_2' => [
        'title' => 'Maklumat Peranan',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatibus, deserunt est debitis. Unde
            earum consectetur, voluptatem quo minus facere vitae, dolor nostrum tenetur nemo voluptatibus! Provident
            nihil, sit harum?'
    ],
    'field' => [
        'name' => 'Nama Penuh',
        'username' => 'Nama Pengguna (Untuk Log Masuk)',
        'user_title_id' => 'Pangkat',
        'email' => 'Emel',
        'email_register' => 'Emel (Untuk Log Masuk)',
        'gender_id' => 'Jantina',
        'password' => 'Kata Laluan',
        'old_password' => 'Kata Laluan Asal',
        'password_confirmation' => 'Pengesahan Kata Laluan',
        'profile_image' => 'Gambar Profil',
        'nric' => 'No. Kad Pengenalan',
        'old_nric' => 'No. Kad Pengenalan Lama',
        'passport' => 'No. Passport',
        'army_police_number' => 'No. Tentera / Polis',
        'captcha' => 'Captcha',
        'is_agree' => "Saya bersetuju dengan semua maklumat yang diberikan dan bertanggungjawab di atas semua risiko yang berkaitan.",
    ]
];
