<?php
return [
    'admin' => 'Pentadbiran',
    'announcement' => 'Pengumuman',
    'auth' => 'Authentication',
    'complaint' => 'Maklumat / Aduan',
    'feed' => 'Suapan',
    'inquiry' => 'Pertanyaan',
    'people' => 'Orang',
    'suspect' => 'OYD',
];
