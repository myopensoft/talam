<?php

return [
    'model' => 'Peranan',
    'index' => 'Senarai peranan',
    'create' => 'Cipta peranan baharu',
    'show' => 'Lihat',
    'edit' => 'Kemaskini',
    'update' => 'Simpan',
    'delete' => 'Hapus',
];
