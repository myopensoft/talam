<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Katalaluan mestilah sekurang-kurangnya 6 aksara dan sama dengan pengesahan.',
    'reset' => 'Kata laluan anda telah diset semula!',
    'sent' => 'Anda akan menerima email untuk set semula kata laluan jika akaun anda adalah sah. Mohon untuk periksa email anda dalam jangka masa 1-2 minit.',
    'token' => 'Token set semula kata laluan ini tidak sah.',
    'user' => "Akaun anda tidak wujud di dalam sistem.",

];
