@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Project Type
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($projectType, ['route' => ['z.projectTypes.update', $projectType->id], 'method' => 'patch']) !!}

                        @include('z.project_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection