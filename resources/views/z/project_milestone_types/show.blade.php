@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Project Milestone Type
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('z.project_milestone_types.show_fields')
                    <a href="{{ route('z.projectMilestoneTypes.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
