@extends('layouts.public')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>Bagaimana Saya Boleh Membuat Aduan Rasuah?</h2>
            <p>Anda boleh membuat aduan menggunakan kaedah 5W1H seperti dibawah:</p>
        </div>
    </div>
    <div class="row m-b-md">
        <div class="col-md-12">
            <h3>Siapa</h3>
            <p>Menyatakan siapakah identiti yang terlibat.</p>
        </div>
        <div class="col-md-12">
            <h3>Bagaimana dilakukan</h3>
            <p>Bagaimanakah perlakuan tersebut dilakukan.</p>
        </div>
        <div class="col-md-12">
            <h3>Dimana</h3>
            <p>Dimanakah lokasi kejadian perlakuan tersebut berlaku.</p>
        </div>
        <div class="col-md-12">
            <h3>Apa yang berlaku</h3>
            <p>Keterangan tentang apa sebenarnya yang berlaku.</p>
        </div>
        <div class="col-md-12">
            <h3>Bila</h3>
            <p>Tarikh dan masa kejadian tersebut berlaku.</p>
        </div>
        <div class="col-md-12">
            <h3>Kenapa dilakukan</h3>
            <p>Nyatakan kepentingan / keuntungan perlakuan tersebut dilakukan kepada pelaku.</p>
        </div>
    </div>
@endsection
