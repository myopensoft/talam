@extends('layouts.app')
@section('page_title')
    {{ __(strtolower('portal.banners.edit')) }}
@endsection
@section('page_module')
    {{ __(strtolower('portal.banners.model')) }}
@endsection
@section('content')
    <section class="content-header">
        <h1></h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title"><h5>{{ __(strtolower('portal.banners.model')) }} : {{$country->name}}</h5></div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <b>{{ __(strtolower('portal.banners.model')) }} Information</b><br>
                                        <i class="fa fa-info-circle text-warning"></i>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatibus, deserunt est debitis. Unde
                                            earum consectetur, voluptatem quo minus facere vitae, dolor nostrum tenetur nemo voluptatibus! Provident
                                            nihil, sit harum?
                                        </small>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            @include('portal.banners.show_fields')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-footer">
                                <div class="row">
                                    <!-- Submit Field -->
                                    <div class="form-group col-sm-12 m-b-none">
                                        <a href="{!! route('portal.banners.index') !!}" class="btn btn-default">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection