@extends('layouts.app')
@section('page_title')
    {{ __(strtolower('portal.banners.create')) }}
@endsection
@section('page_module')
    {{ __(strtolower('portal.banners.model')) }}
@endsection
@section('content')
    <section class="content-header">
        <h1></h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'portal.banners.store']) !!}
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title"><h5>{{ __(strtolower('portal.banners.model')) }}</h5></div>
                            <div class="ibox-content">
                                 @include('portal.banners.fields')
                            </div>
                            <div class="ibox-footer">
                                <div class="row">
                                    <!-- Submit Field -->
                                    <div class="form-group col-sm-12 m-b-none">
                                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                        <a href="{!! route('portal.banners.index') !!}" class="btn btn-default">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
