<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title"><h5>List of {{ __(strtolower('portal.banners.model')) }}</h5></div>
            <div class="ibox-content">
                <div class="table-responsive">
                    {!! $dataTable->table(['width' => '100%']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    {!! $dataTable->scripts() !!}
@endsection