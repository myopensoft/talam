@extends('layouts.app')
@section('content')
    <div class="wrapper wrapper-content animated fadeIn">
        <div class="">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="text-center m-b-md">Jumlah Maklumat</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="ibox float-e-margins b-r-lg">
                                <div class="ibox-content b-r-lg">
                                    <h1 class="text-center">
                                        {{ number_format($complaint_today->total ?? 0)}}
                                    </h1>
                                </div>
                            </div>
                            <p class="text-center">Hari</p>
                        </div>
                        <div class="col-md-4">
                            <div class="ibox float-e-margins b-r-lg">
                                <div class="ibox-content b-r-lg">
                                    <h1 class="text-center">
                                        {{ number_format($complaint_month->total ?? 0) }}
                                    </h1>
                                </div>
                            </div>
                            <p class="text-center">Bulan</p>
                        </div>
                        <div class="col-md-4">
                            <div class="ibox float-e-margins b-r-lg">
                                <div class="ibox-content b-r-lg">
                                    <h1 class="text-center">
                                        {{ number_format($complaint_year->total ?? 0) }}
                                    </h1>
                                </div>
                            </div>
                            <p class="text-center">Tahun</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 m-t-xl">
                    <div class="row dashboard-stat">
                        <div class="col-lg-4 col-md-4">
                            <div class="ibox float-e-margins status task b-r-lg">
                                <div class="ibox-content b-r-lg">
                                    <a href="{{route('complaints.inbox.index')}}">
                                        <div class="visual">
                                            <i class="fa fa-exclamation-circle"></i>
                                        </div>
                                        <div class="details">
                                            <h1 class="no-margins text-right">{{number_format($my_task)}}</h1>
                                            <h2 class="text-right">Tugasan Saya</h2>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="ibox float-e-margins status b-r-lg {{$overdue->total != 0 ? 'danger' : 'task'}}">
                                <div class="ibox-content b-r-lg">
                                    <a href="{{route('complaints.inbox.index')}}">
                                        <div class="visual">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                        <div class="details">
                                            <h1 class="no-margins text-right">{{number_format($overdue->total)}}</h1>
                                            <h2 class="text-right">Overdue</h2>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="ibox float-e-margins status kiv b-r-lg">
                                <div class="ibox-content b-r-lg">
                                    <a href="{{route('complaints.inbox.index')}}">
                                        <div class="visual">
                                            <i class="fa fa-stack-exchange"></i>
                                        </div>
                                        <div class="details">
                                            <h1 class="no-margins text-right">{{number_format($complaint_stat[6] ?? 0)}}</h1>
                                            <h2 class="text-right">KIV</h2>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-12 m-t">
                            <div class="row dashboard-stat">
                                <div class="col-lg-4 col-md-4">
                                    <div class="ibox float-e-margins status raw b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <a href="{{route('complaints.raw.index')}}">
                                                <div class="visual">
                                                    <i class="fa fa-exclamation-triangle"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[2] ?? 0)}}</h1>
                                                    <h2 class="text-right">Raw</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="ibox float-e-margins status inbox b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <a href="{{route('complaints.inbox.index')}}">
                                                <div class="visual">
                                                    <i class="fa fa-inbox"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[3] ?? 0)}}</h1>
                                                    <h2 class="text-right">Inbox</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="ibox float-e-margins status suggest-nfa b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <a href="{{route('complaints.nfa-suggestion.index')}}">
                                                <div class="visual">
                                                    <i class="fa fa-bell"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[4] ?? 0)}}</h1>
                                                    <h2 class="text-right">Cadang NFA</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="ibox float-e-margins status pre-cris b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <a href="{{route('complaints.pre-cris.index')}}">
                                                <div class="visual">
                                                    <i class="fa fa-unlink"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[8] ?? 0)}}</h1>
                                                    <h2 class="text-right">Pre-CRIS</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="ibox float-e-margins status cris b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            @hasanyrole('admin-system|oc|poc|kc|ku')
                                            <a href="{{route('complaints.cris.index')}}">
                                                <div class="visual">
                                                    <i class="fa fa-link"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[9] ?? 0)}}</h1>
                                                    <h2 class="text-right">CRIS</h2>
                                                </div>
                                            </a>
                                            @else
                                                <div class="visual">
                                                    <i class="fa fa-pencil"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[9] ?? 0)}}</h1>
                                                    <h2 class="text-left">CRIS</h2>
                                                </div>
                                                @endhasanyrole
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="ibox float-e-margins status refer b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <a href="{{route('complaints.refer.index')}}">
                                                <div class="visual">
                                                    <i class="fa fa-building"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[7] ?? 0)}}</h1>
                                                    <h2 class="text-right">Rujuk Jabatan</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="{{auth()->user()->hasAnyRole('oc|kc|ku') ? 'col-lg-4 col-md-6' : 'col-lg-4 col-md-4'}}">
                                    <div class="ibox float-e-margins status contact-note b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <a href="{{route('complaints.qaa.index')}}">
                                                <div class="visual">
                                                    <i class="fa fa-question-circle-o"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[5] ?? 0)}}</h1>
                                                    <h2 class="text-right">Pertanyaan</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="ibox float-e-margins status contact-note b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <a href="#">
                                                <div class="visual">
                                                    <i class="fa fa-sticky-note"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format($complaint_stat[1] ?? 0)}}</h1>
                                                    <h2 class="text-right">Contact Note</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="ibox float-e-margins status nfa b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <a href="#">
                                                <div class="visual">
                                                    <i class="fa fa-trash-o"></i>
                                                </div>
                                                <div class="details">
                                                    <h1 class="no-margins text-right">{{number_format(($complaint_stat[10] ?? 0) + ($complaint_stat[11] ?? 0) + ($complaint_stat[12] ?? 0) + ($complaint_stat[13] ?? 0))}}</h1>
                                                    <h2 class="text-right">NFA & SPAM</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @hasanyrole('oc|kc|ku')
                        <div class="col-lg-12 m-t">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="ibox float-e-margins b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <h3><i class="fa fa-exclamation-triangle text-danger"></i> Urgent
                                                RAW</h3>
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th width="30%">No. Maklumat</th>
                                                    <th>Tajuk</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($complaints as $complaint)
                                                    <tr>
                                                        <td style="vertical-align: middle">
                                                            <a href="{{ route('complaints.show', $complaint->slug) }}">
                                                                {{ $complaint->reference_number }}
                                                            </a>
                                                        </td>
                                                        <td>{{ $complaint->title }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{ $complaints->links() }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="ibox float-e-margins b-r-lg">
                                        <div class="ibox-content b-r-lg">
                                            <h3><i class="fa fa-inbox text-primary"></i> Agihan Pegawai</h3>
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th width="10%" class="text-right">#</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($users as $user)
                                                    <tr>
                                                        <td style="vertical-align: middle">{{ $user->name }}</td>
                                                        <td class="text-right"><h2 class="no-margins"><a
                                                                    href="">{{ $user->st_inbox }}</a></h2></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{ $users->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endhasanyrole
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
      $(document).ready(function () {
        var ctx = document.getElementById('myChart')
        var myChart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: [ 'Jan', 'Feb', 'Mac', 'Apr', 'Mei', 'Jun', 'Jul', 'Ogo', 'Sep', 'Okt', 'Nov', 'Dis' ],
            datasets: [
              {
                label: 'Hantar CRIS.\n',
                data: [ 2, 4, 1, 0, 0, 1, 9, 3, 1, 3, 1, 0, 0, 1 ],
                backgroundColor: [
                  'RGBA(222, 109, 36, 0.5)'
                ],
                borderColor: [
                  'RGBA(222, 109, 36, 1.00)'
                ]
              },
              {
                label: 'Jumlah Aduan.\n',
                data: [ 12, 19, 3, 5, 2, 3, 12, 19, 3, 5, 2, 3, 4, 1 ],
                backgroundColor: [
                  'RGBA(40, 178, 148, 0.5)'
                ],
                borderColor: [
                  'RGBA(40, 178, 148, 1.00)'
                ]
              }
            ]
          },
          options: {
            elements: {
              line: {
                tension: 0.4
              }
            },
            scales: {
              yAxes: [ {
                ticks: {
                  beginAtZero: true
                }
              } ]
            }
          }
        })
      })
    </script>
@endsection
