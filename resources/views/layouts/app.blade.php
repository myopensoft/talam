<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="{{'skin'.(auth()->user()->skin ?? 1)}}">
<div id="wrapper">
    @include('layouts.nav')
    <div id="page-wrapper" class="bg-muted">
        <div class="row border-bottom">
            @include('layouts.navbar')
        </div>
        @if(!Request::is('/'))
            @include('layouts.breadcrumb')
        @endif
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-sm-12 m-b-md">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @include('flash::message')
                @include('adminlte-templates::common.errors')
                @yield('content')
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>

@include('publics.modal_contact_us')

<!-- Scripts -->
<script src="{{ mix('js/manifest.js') }}" type="text/javascript"></script>
<script src="{{ mix('js/vendor.js') }}" type="text/javascript"></script>
<script src="{{ mix('js/script.js') }}" type="text/javascript"></script>

@section('scripts')
@show
@section('scripts2')
@show
</body>
</html>
