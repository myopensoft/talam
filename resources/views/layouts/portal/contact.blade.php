<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>{{__('portal.about.title')}}</h1>
                <p>{{__('portal.about.subtitle')}}</p>
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-sm-5 col-sm-offset-1 col-md-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">{{__('portal.about.ministry')}}</span></strong><br/>
                    {{__('portal.about.address_1')}} <br>
                    {{__('portal.about.address_2')}} <br>
                    {{__('portal.about.address_3')}} <br>
                    <abbr title="Phone"><i class="fa fa-phone"></i></abbr> : {{__('portal.about.phone')}} <br>
                    <abbr title="Fax"><i class="fa fa-fax"></i></abbr> : {{__('portal.about.fax')}} <br>
                    <abbr title="Web"><i class="fa fa-globe"></i></abbr> : {{__('portal.about.web')}}
                </address>
            </div>
            <div class="col-sm-5 col-lg-4">
                <div class="row">
                    <div class="col-md-4">
                        <h5>About</h5>
                        <a href="#">Our Mission</a>
                        <a href="#">KPDNKK</a>
                    </div>
                    <div class="col-md-4">
                        <h5>Support</h5>
                        <a href="#">FAQ</a>
                        <a href="#">Help Center</a>
                        <a href="#">T&C</a>
                        <a href="#">Privacy Policy</a>
                    </div>
                    <div class="col-md-4">
                        <h5>Contact</h5>
                        <a href="#">Email us</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>{{ __('portal.footer.copyright') }} &copy; 2010
                        - {{ date('Y') }}</strong><br/>{{ __('portal.footer.owner') }}</p>
            </div>
        </div>
    </div>
</section>