<!DOCTYPE html>
<html lang="en">
<head>
    <title>Complain Management System (CMS)</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Selamat Datang ke Sistem Pengurusan Aduan SPRM" />
    <script>
      addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
      }, false);

      function hideURLbar() {
        window.scrollTo(0, 1);
      }
    </script>
    <!-- //Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/portal/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/portal/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <link href="css/portal/font-awesome.min.css" rel="stylesheet">
    <!-- Font-Awesome-Icons-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
{{--    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext"--}}
{{--          rel="stylesheet">--}}
    <!-- //Web-Fonts -->
</head>

<body>
<!-- main banner -->
<div class="main-top" id="home">
    <!-- header -->
    <header>

    </header>
    <!-- //header -->

    <!-- banner -->

    <div class="banner_w3lspvt position-relative">
        <div class="container-fluid">
            <div class="header d-lg-flex justify-content-between align-items-center py-3 px-sm-3">
                <!-- logo -->
                <div id="logo">
                    <a href="index.html"><img src="img/portal/CSMlogo_mini.png" alt="" class="img-fluid"></a>
                </div>
                <div class="d-flex mt-lg-1 mt-sm-2 mt-3 justify-content-center">
                    <!-- search -->

                    <!-- //search -->
                    <!-- download -->
                    <p> Download Our Mobile Apps:
                        <a class="dwn-w3ls btn" href="#" target="_blank">
                            <span class="fa fa-apple"></span>

                        </a>
                        <a class="dwn-w3ls btn" href="#" target="_blank">
                            <span class="fa fa-android"></span>

                        </a></p>
                    <!-- //download -->
                </div>
            </div>
        </div>

        <div class="container">

            <div class="d-md-flex">
                <div class="w3ls_banner_txt">
                    <img src="img/portal/CSMlogo.png" alt="" class="img-fluid" style="width: 65%;">
                    <p class="text-sty-banner">Salur dan adukan kepada kami maklumat salah laku rasuah yang berlaku. </p>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Add New Product
                    </button>
                    <a href="login" class="btn button-style mt-md-5 mt-4">Laporkan Aduan Anda <i class="fa fa-chevron-right"></i></a>
                </div>
                <!-- <div class="banner-img">
                    <img src="images/banner.png" alt="" class="img-fluid">
                </div> -->
            </div>
        </div>
    </div>
</div>
@include('auth.modal_login')
<div class="copy-bottom bg-li py-4 border-top">
    <div class="container-fluid">
        <div class="d-md-flex px-md-3 position-relative text-center">
            <!-- footer social icons -->
            <div class="social-icons-footer mb-md-0 mb-3">
                <ul class="list-unstyled">
                    <li>
                        <a href="#">
                            <span class="fa fa-facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-google-plus"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-instagram"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- //footer social icons -->
            <!-- copyright -->
            <div class="copy_right mx-md-auto mb-md-0 mb-3">
                <p class="text-bl let">© 2019 MACC. All rights reserved | Suruhanjaya Pencegahan Rasuah Malaysia
                    <!-- <a href="http://w3layouts.com/" target="_blank">W3layouts</a> -->
                </p>
            </div>
            <!-- //copyright -->
        </div>
    </div>
</div>
<!-- //copyright bottom -->
</body>

</html>