<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top navbar-scroll" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img alt="image" class="" height="40px"
                                                      src="{{asset('img/logo-inverse.png')}}"/></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- BEGIN LANGUAGE DROPDOWN -->
                    <li>
                        <a href="javascript:;" style="padding: 15px; font-size: 13px;"
                           class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <span class="username username-hide-on-mobile"
                                  style="font-size: 13px; text-transform: uppercase;"> {{ Config::get('app.locale') }} </span>
                            <i class="fa fa-angle-down" style="font-size: 13px;"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a onclick="changeLanguage('en')" style="cursor: pointer;">{{ __('i18n.en') }} </a>
                            </li>
                            <li>
                                <a onclick="changeLanguage('ms-MY')"
                                   style="cursor: pointer;">{{ __('i18n.ms-MY') }} </a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="page-scroll" href="#page-top">Home</a></li>
                    <li><a class="page-scroll" href="#about">About</a></li>
                    <li><a class="page-scroll" href="#consumer">Consumer</a></li>
                    <li><a class="page-scroll" href="#industry">Industry</a></li>
                    <li><a class="page-scroll" href="#contact">Contact</a></li>
                    <li><a class="page-scroll" href="/login">Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>