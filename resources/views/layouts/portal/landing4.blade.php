<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Selamat Datang ke Sistem Pengurusan Aduan SPRM"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <script>
        addEventListener('load', function () {
            setTimeout(hideURLbar, 0)
        }, false)

        function hideURLbar() {
            window.scrollTo(0, 1)
        }
    </script>
    <!-- //Meta tag Keywords -->

    <link rel="stylesheet" href="css/portal/bootstrap.css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="css/portal/style.css" type="text/css" media="all"/>
    <link href="css/portal/font-awesome.min.css" rel="stylesheet">
</head>
<body class="{{'skin'.(auth()->user()->skin ?? 1)}}">
<div class="main-top" id="home">
    <header>

    </header>
    <div class="banner_w3lspvt position-relative">
        <div class="container-fluid">
            <div class="d-flex mb-3 flex-column flex-lg-row justify-content-between p-3">
                <div class="p-2 order-1 order-lg-0">
                    <div id="logo">
                        <a href="/"><img src="img/portal/CSMlogo_mini.png" alt="" class="img-fluid"></a>
                    </div>
                </div>
                <div class="p-2 d-flex order-0 order-lg-1" style="justify-content: space-evenly !important;">
                    <a href="#addressModal" class="" data-toggle="modal" data-target="#addressModal">
                        <i class="fa fa-building fa-2x mr-lg-5 mr-0"
                           style="color:#FFFF;"
                           title="Alamat Cawangan"></i>
                    </a>
                    <a href="#contactUsModal" type="button" class="" data-toggle="modal" data-target="#contactUsModal">
                        <i class="fa fa-phone fa-2x mr-lg-5 mr-0"
                           style="color:#FFFF;"
                           title="Hubungi Kami"></i>
                    </a>
                    <a href="#howtomakereport" type="button" class="" data-toggle="modal" data-target="#howtomakereport">
                        <i class="fa fa-book fa-2x mr-lg-5 mr-0"
                           style="color:#FFFF;"
                           title="Panduan Mengadu"></i>
                    </a>
                    <a href="#manual" type="button" class="" data-toggle="modal" data-target="#manual">
                        <i class="fa fa-file-o fa-2x mr-lg-5 mr-0"
                           style="color:#FFFF;"
                           title="Manual Pengguna"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="d-md-flex">
                <div class="w3ls_banner_txt">
                    <img src="img/cms-logo.png" alt="" class="img-fluid" style="width: 65%;">
                    <p class="text-sty-banner">Salur dan adukan kepada kami maklumat salah laku rasuah yang
                        berlaku. </p>
                    @if (auth()->user())
                        <a href="/public" class="btn button-style mt-md-5 mt-4">
                            Kembali ke dashboard <i class="fa fa-chevron-right"></i>
                        </a>
                    @else
                        <button type="button" href="login" class="btn button-style mt-md-5 mt-4" data-toggle="modal"
                                data-backdrop="static" data-keyboard="false"
                                data-target="#loginModal">
                            Laporkan Aduan Anda <i class="fa fa-chevron-right"></i>
                        </button>
                    @endif
                </div>
            </div>
            {{-- <button type="button" class="btn button-style btn-xs mt-md-5 mt-4"
                    style="padding: 8px 16px !important; background:#021D64" data-toggle="modal"
                    data-backdrop="static" data-keyboard="false"
                    data-target="#addressModal">Alamat Cawangan <i class="fa fa-chevron-right"></i>
            </button> --}}
        </div>
    </div>
</div>
</div>

<div class="copy-bottom bg-li py-4 border-tosp">
    <div class="container-fluid">
        <div class="d-md-flex px-md-3 position-relative text-center">
            <div class="social-icons-footer">
                <p class="text-bl" style="padding-top: 12px">Jumlah Pelawat: {{$visitor_counter}}</p>
            </div>
            <div class="copy_right mx-md-auto mb-md-0 mb-3">
                <p class="text-bl let" style="padding-top: 12px">© {{date('Y')}} SPRM. Hak cipta terpelihara |
                    Suruhanjaya Pencegahan Rasuah Malaysia</p>
            </div>
            <div class="social-icons-footer">
                Dapatkan MACCMobile
                <a href="https://apps.apple.com/my/app/maccmobile/id1090338943" target="_blank">
                    <img src="{{asset('img/appstore-logo.png')}}" width="150px" alt="">
                </a>
                <a href="https://play.google.com/store/apps/details?id=my.gov.onegovappstore.MACCMobile.keystore&hl=en"
                   target="_blank">
                    <img src="{{asset('img/playstore-logo.png')}}" width="150px" alt="">
                </a>
            </div>
        </div>
    </div>
</div>

@include('auth.modal_login')
@include('publics.modal_contact_us')
@include('publics.modal_address')
@include('publics.modal_manual')
@include('publics.modal_how_to_make_report')

<script src="{{ mix('/js/landing.js') }}" type="text/javascript"></script>
<script>
    $(function () {
        @if (session('status') || count($errors) > 0)
        $('#loginModal').modal('show')
        @endif

        console.log('loaded')
        loadAddresses()

        $('#state_id').on('change', function () {
            console.log('change')
            loadAddresses()
        })

        function nl2br(str, is_xhtml) {
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>'
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2')
        }

        function loadAddresses() {
            $('table#address tbody').empty()

            $.ajax({
                url: '/api/directory_branches?search=state_id:' + ($('#state_id').val() != '' ? $('#state_id').val() : '16'),
                type: 'GET',
                dataType: 'json', // lowercase is always preferered though jQuery does it, too.
                success: function (data) {
                    console.log('success to save data', data)
                    if (Object.keys(data.data).length > 0) {
                        var markup = ''
                        console.log(Object.keys(data.data).length)
                        $.each(data.data, function (index, datum) {
                            markup += '<tr>\n' +
                                '<td>' + (index + +1) + '</td>\n' +
                                '<td>' + nl2br(datum.address) +
                                '<br><br>Tel: ' + datum.phone_number +
                                '<br>Fax: ' + datum.fax_number +
                                '<br><br><a target="_blank" href="https://maps.google.com.my/maps?q=' + datum.geo_lat + ',' + datum.geo_lng + '"><i class="fa fa-globe"></i> Buka Peta</a>' +
                                '</td>\n' +
                                '</tr>'
                        })
                        $('table#address tbody').append(markup)
                    }
                }
            })
        }
    })
</script>
</body>
</html>
