@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Credential
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($credential, ['route' => ['project.credentials.update', $credential->id], 'method' => 'patch']) !!}

                        @include('project.credentials.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection