<div class="table-responsive">
    <table class="table" id="credentials-table">
        <thead>
            <tr>
                <th>Project Id</th>
        <th>Type Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Host</th>
        <th>Username</th>
        <th>Password</th>
        <th>Expired Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($credentials as $credential)
            <tr>
                <td>{{ $credential->project_id }}</td>
            <td>{{ $credential->type_id }}</td>
            <td>{{ $credential->name }}</td>
            <td>{{ $credential->description }}</td>
            <td>{{ $credential->host }}</td>
            <td>{{ $credential->username }}</td>
            <td>{{ $credential->password }}</td>
            <td>{{ $credential->expired_date }}</td>
                <td>
                    {!! Form::open(['route' => ['project.credentials.destroy', $credential->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('project.credentials.show', [$credential->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('project.credentials.edit', [$credential->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
