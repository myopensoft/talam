<!-- Project Id Field -->
<div class="form-group">
    {!! Form::label('project_id', 'Project Id:') !!}
    <p>{{ $milestone->project_id }}</p>
</div>

<!-- Milestone Type Id Field -->
<div class="form-group">
    {!! Form::label('milestone_type_id', 'Milestone Type Id:') !!}
    <p>{{ $milestone->milestone_type_id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $milestone->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $milestone->description }}</p>
</div>

<!-- Milestone Date Field -->
<div class="form-group">
    {!! Form::label('milestone_date', 'Milestone Date:') !!}
    <p>{{ $milestone->milestone_date }}</p>
</div>

