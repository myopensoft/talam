<div class="table-responsive">
    <table class="table" id="milestones-table">
        <thead>
            <tr>
                <th>Project Id</th>
        <th>Milestone Type Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Milestone Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($milestones as $milestone)
            <tr>
                <td>{{ $milestone->project_id }}</td>
            <td>{{ $milestone->milestone_type_id }}</td>
            <td>{{ $milestone->name }}</td>
            <td>{{ $milestone->description }}</td>
            <td>{{ $milestone->milestone_date }}</td>
                <td>
                    {!! Form::open(['route' => ['project.milestones.destroy', $milestone->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('project.milestones.show', [$milestone->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('project.milestones.edit', [$milestone->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
