@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Milestone
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($milestone, ['route' => ['project.milestones.update', $milestone->id], 'method' => 'patch']) !!}

                        @include('project.milestones.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection