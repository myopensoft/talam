<div class="table-responsive">
    <table class="table" id="tasks-table">
        <thead>
            <tr>
                <th>Project Id</th>
        <th>User Id</th>
        <th>Title</th>
        <th>Description</th>
        <th>Open Date</th>
        <th>Close Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tasks as $task)
            <tr>
                <td>{{ $task->project_id }}</td>
            <td>{{ $task->user_id }}</td>
            <td>{{ $task->title }}</td>
            <td>{{ $task->description }}</td>
            <td>{{ $task->open_date }}</td>
            <td>{{ $task->close_date }}</td>
                <td>
                    {!! Form::open(['route' => ['project.tasks.destroy', $task->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('project.tasks.show', [$task->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('project.tasks.edit', [$task->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
