<!-- Project Id Field -->
<div class="form-group">
    {!! Form::label('project_id', 'Project Id:') !!}
    <p>{{ $task->project_id }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $task->user_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $task->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $task->description }}</p>
</div>

<!-- Open Date Field -->
<div class="form-group">
    {!! Form::label('open_date', 'Open Date:') !!}
    <p>{{ $task->open_date }}</p>
</div>

<!-- Close Date Field -->
<div class="form-group">
    {!! Form::label('close_date', 'Close Date:') !!}
    <p>{{ $task->close_date }}</p>
</div>

