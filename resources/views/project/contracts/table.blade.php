<div class="table-responsive">
    <table class="table" id="contracts-table">
        <thead>
            <tr>
                <th>Project Id</th>
        <th>Type Id</th>
        <th>Start Date</th>
        <th>End Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($contracts as $contract)
            <tr>
                <td>{{ $contract->project_id }}</td>
            <td>{{ $contract->type_id }}</td>
            <td>{{ $contract->start_date }}</td>
            <td>{{ $contract->end_date }}</td>
                <td>
                    {!! Form::open(['route' => ['project.contracts.destroy', $contract->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('project.contracts.show', [$contract->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('project.contracts.edit', [$contract->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
