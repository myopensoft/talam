<?php

Auth::routes(['verify' => true]);

Route::get('/', 'Portal\Controller@index')->name('landing');

Route::group(['prefix' => 'z'], function () {
    Route::resource('projectMilestoneTypes', 'Z\ProjectMilestoneTypeController', ["as" => 'z']);
    Route::resource('clients', 'Z\ClientController', ["as" => 'z']);
    Route::resource('projectOrganizations', 'Z\ProjectOrganizationController', ["as" => 'z']);
    Route::resource('projectTypes', 'Z\ProjectTypeController', ["as" => 'z']);
});


Route::resource('users', 'UserController');
