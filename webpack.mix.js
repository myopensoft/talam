let mix = require('laravel-mix')

mix
    .options({ processCssUrls: false, __dirname: false })
    .extract([
        'vue'
    ])
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/landing.scss', 'public/css')
    .styles([
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'node_modules/font-awesome/css/font-awesome.css',
        'node_modules/animate/animate.css',
        'node_modules/toastr/build/toastr.min.css',
        'node_modules/jquery-ui-dist/jquery-ui.css',
        'node_modules/jasny-bootstrap/dist/css/jasny-bootstrap.min.css',
        'resources/assets/css/chosen/bootstrap-chosen.css',
        'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css',
        'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
        'node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.css',
        'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
        'node_modules/select2/dist/css/select2.css',
        'node_modules/dropzone/dist/min/dropzone.min.css',
        'node_modules/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
        'public/css/app.css'
    ], 'public/css/style.css')
    .copy('node_modules/bootstrap/dist/css/bootstrap.css.map', 'public/css')
    .copy('node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css.map', 'public/css')
    .js('resources/assets/js/app.js', 'public/js')
    .scripts([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        'node_modules/metismenu/dist/metisMenu.js',
        'node_modules/jquery-slimscroll/jquery.slimscroll.js',
        'node_modules/jquery-ui-dist/jquery-ui.js',
        'node_modules/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
        'node_modules/jquery-sparkline/jquery.sparkline.js',
        'resources/assets/js/chosen.jquery.js',
        'node_modules/toastr/toastr.js',
        'node_modules/moment/moment.js',
        'node_modules/datatables.net/moment.js',
        'node_modules/moment/moment.js',
        'node_modules/datatables.net/js/jquery.dataTables.js',
        'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js',
        'node_modules/datatables.net-buttons/js/dataTables.buttons.js',
        'node_modules/datatables.net-buttons-bs/js/buttons.bootstraps.js',
        'node_modules/datatables.net-buttons/js/buttons.colVis.js',
        'node_modules/select2/dist/js/select2.min.js',
        'vendor/yajra/laravel-datatables-buttons/src/resources/assets/buttons.server-side.js',
        'node_modules/dropzone/dist/min/dropzone.min.js',
        'node_modules/chart.js/dist/Chart.min.js',
        'node_modules/sweetalert/dist/sweetalert.min.js',
        'node_modules/jquery-blockui/jquery.blockUI.js',
        'resources/assets/js/portal/popper.min.js',
        'resources/assets/js/inspinia.js',
        'resources/assets/js/app.js',
        'resources/assets/js/people.js'
    ], 'public/js/script.js')
    // .copy('node_modules/metismenu/dist/metisMenu.js.map', 'public/js')
    // .copy('node_modules/jasny-bootstrap/dist/css/jasny-bootstrap.css.map', 'public/js')
    .scripts([
        'resources/assets/js/portal/jquery-3.2.1.min.js',
        'resources/assets/js/portal/popper.min.js',
        'resources/assets/js/portal/bootstrap.min.js',
    ], 'public/js/landing.js')
    .copy('resources/assets/img', 'public/img')
    .copy('resources/assets/file', 'public/file')
    .copy('node_modules/jquery-ui-dist/images', 'public/css/images')
    .copy('resources/assets/css/chosen/', 'public/css/')
    .copy('node_modules/font-awesome/fonts/', 'public/fonts/')
    .version()
    .sourceMaps()
